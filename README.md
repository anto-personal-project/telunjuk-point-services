# telunjuk-point-services

Telunjuk-Poin-Service is an API for Telunjuk App. It consists of two main service

  - Point Service
  - Documentations

### Tech

Telunjuk-Poin-Service uses a number of open source projects to work properly:

* [PHP] - For Runtime Environment
* [Slim] - For Service Framework
* PostgreSQL - For Storing Database
* Postman - For Documenting Services

### Installation

Telunjuk-Poin-Service requires [PHP] v7.4.6+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ composer install
$ php -S localhost:8080 -t public public/index.php
```
