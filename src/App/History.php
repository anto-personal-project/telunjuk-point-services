<?php

namespace App;

// use Psr\Log\LogLevel;

use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

use Spot\EventEmitter;
use Tuupola\Base62;
use Psr\Log\LogLevel;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;



class History extends \Spot\Entity
{
    protected static $table = "history";

    public static function fields()
    {
        return [
            "id" => ["type" => "integer", "unsigned" => true, "primary" => true, "autoincrement" => true],
            "history_id" => ["type" => "guid", "required" => true, "unique" => true],
            "point_id" => ["type" => "integer", "required" => true,], // point foreign key
            "amount" => ["type" => "decimal", "required" => true, "value" => 0], // can be + or -
            "balance" => ["type" => "decimal", "required" => true, "value" => 0], // can be + or -
            "type" => ["type" => "smallint", "required" => true, "value" => 1], // deduct or topup
            "description" => ["type" => "string", "length" => 255],
            "ref_no" => ["type" => "string", "length" => 255], // transaction guid or bank trasaction no 
            "ref_date" => ["type" => "datetime"], // transaction guid or bank trasaction no 
            "status" => ["type" => "smallint", "length" => 6, "required" => true],
            "created_at"   => ["type" => "datetime", "value" => new \DateTime(), "required" => true],
        ];
    }

    public static function events(EventEmitter $emitter)
    {
        $emitter->on("beforeInsert", function (Entity $entity, Mapper $mapper) {
            // $entity->uid = (new Base62)->encode(random_bytes(9));
            $uuid = Uuid::uuid4();
            $entity->history_id = $uuid;
        });
    }

    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'point' => $mapper->belongsTo($entity, 'App\Point', 'point_id'),
        ];
    }
    
    public function timestamp()
    {
        return $this->created_at->getTimestamp();
    }

    public function etag()
    {
        return md5($this->history_id . $this->timestamp());
    }

    public function clear()
    {
        $this->data([
            "history_id" => null,
            "point_id" => null, // point foreign key
            "amount" => null, // can be + or -
            "balance" => null, // can be + or -
            "type" => null, // deduct or topup
            "description" => null,
            "ref_no" => null, // transaction guid or bank trasaction no 
            "ref_date" => null, // transaction guid or bank trasaction no 
            "status" => null,
        ]);
    }
}
