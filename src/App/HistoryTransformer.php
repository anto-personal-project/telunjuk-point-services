<?php

namespace App;

use App\History;
use League\Fractal;

class HistoryTransformer extends Fractal\TransformerAbstract
{
    private $_middleUrl = "";
    
    public function setMiddleUrl($url) {
        $this->_middleUrl = $url;
    }

    public function transform(History $history)
    {
        return [
            "history_id" => (string) $history->history_id,
            "point_id" => (string) $history->point_id, // point foreign key
            "amount" => $history->amount, // can be + or -
            "balance" => $history->balance, // can be + or -
            "type" => (integer) $history->type, // deduct or topup
            "description" => (string) $history->description,
            "ref_no" => (string) $history->ref_no, // history guid or bank trasaction no 
            "ref_date" => $history->ref_date == null ? null : (string) $history->ref_no, // history guid or bank trasaction no 
            "status" => (string) $history->status,
            "created_at" => (string) $history->created_at->format('Y-m-d h:i:s'),
       ];
    }
}
