<?php

use App\Point;
use App\PointTransformer;

use Exception\NotFoundException;
use Exception\ForbiddenException;
use Exception\PreconditionFailedException;
use Exception\PreconditionRequiredException;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\DataArraySerializer;

$app->get(getenv("MIDDLE_URL") . "/mypoint", function ($request, $response, $arguments) {
    /* Load existing point using provided id */
    if (false === $point = $this->spot->mapper("App\Point")->first([
        "user_id" => $this->token->decoded->uid
    ])) {
        throw new NotFoundException("Point not found.", 404);
    };

    /* Add Last-Modified and ETag headers to response. */
    $response = $this->cache->withEtag($response, $point->etag());
    $response = $this->cache->withLastModified($response, $point->timestamp());

    /* If-Modified-Since and If-None-Match request header handling. */
    /* Heads up! Apache removes previously set Last-Modified header */
    /* from 304 Not Modified responses. */
    if ($this->cache->isNotModified($request, $response)) {
        return $response->withStatus(304);
    }

    $data["status"] = "ok";    
    /* Serialize the response data. */
    $fractal = new Manager();
    $fractal->setSerializer(new DataArraySerializer);
    $pointTransformer = new PointTransformer();
    $pointTransformer->setMiddleUrl(getenv("MIDDLE_URL"));
    $resource = new Item($point, $pointTransformer);
    $profile = $fractal->createData($resource)->toArray();

    $data = array_merge($data, $profile);
    
    return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
});

