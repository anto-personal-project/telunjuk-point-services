<?php

namespace App;

use App\Point;
use League\Fractal;

class PointTransformer extends Fractal\TransformerAbstract
{
    private $_middleUrl = "";
    
    public function setMiddleUrl($url) {
        $this->_middleUrl = $url;
    }

    public function transform(Point $point)
    {
        return [
            "point_id" => (string)$point->point_id,
            "user_id" => (string) $point->user_id,
            "balance" => $point->balance,
            "last_topup" => $point->last_topup == null ? null :(string) $point->last_topup->format('Y-m-d h:i:s'),
            "last_deduct" => $point->last_deduct == null ? null : (string) $point->last_deduct->format('Y-m-d h:i:s'),
            "status" => (integer) $point->status,
            "created_at"   => (string) $point->created_at->format('Y-m-d h:i:s'),
            "updated_at"   => (string) $point->updated_at->format('Y-m-d h:i:s')
       ];
    }
}
