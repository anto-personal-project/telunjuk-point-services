<?php

use App\Point;
use App\PointTransformer;

use Exception\NotFoundException;
use Exception\ForbiddenException;
use Exception\PreconditionFailedException;
use Exception\PreconditionRequiredException;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\DataArraySerializer;

$app->get(getenv("MIDDLE_URL") . "/point", function ($request, $response, $arguments) {


    $arrOrderBy = ['created_at' => 'created_at'];
    $arrDirection = [0 => 'DESC', 1 => 'ASC'];

    /*
        parameter:
        - p : Page
        - ps : Page Size, default 10 records
        - ob : Order By, default order by created_at field
        - d : Order Direction, default DESC
    */
    $orderBy = $arrOrderBy[$request->getQueryParam("ob", "created_at")];
    $direction = $arrDirection[$request->getQueryParam("d", 1)];
    $pageSize = $request->getQueryParam("ps", 10);

    $point = $this->spot->mapper("App\Point")
    ->all()
    ->order([$orderBy => $direction]);

    $recordCount = $point->count();

    $pageCount = ceil($recordCount / $pageSize);

    $blnPagination = false;

    $page = $request->getQueryParam("p");
    // if ($page <= 0 ) { 
    //     $page = 1;
    // } else if ($page >= $pageCount)  {
    //     $page = $pageCount;
    // } 
    if (is_numeric($page)) {
        $blnPagination = true;
    }

    $page = ($page <= 0) ? 1 : (($page >= $pageCount) ? $pageCount : $page);
    $offset = (($page - 1) <= 0 ? 0 : ($page - 1)) * $pageSize;



    if ($blnPagination) {
        /* Use ETag and date from point with most recent update. */
        $first = $this->spot->mapper("App\Point")
            ->all()
            ->order([$orderBy => $direction])
            ->offset($offset)
            ->limit($pageSize)
            ->first();
    } else {
        /* Use ETag and date from point with most recent update. */
        $first = $this->spot->mapper("App\Point")
            ->all()
            ->order([$orderBy => $direction])
            ->first();
    }

    /* Add Last-Modified and ETag headers to response when atleast on point exists. */
    if ($first) {
        // throw new ForbiddenException("ETag : " . $first->timestamp(), 403);
        $response = $this->cache->withEtag($response, $first->etag());
        $response = $this->cache->withLastModified($response, $first->timestamp());
    }


    /* If-Modified-Since and If-None-Match request header handling. */
    /* Heads up! Apache removes previously set Last-Modified header */
    /* from 304 Not Modified responses. */
    if ($this->cache->isNotModified($request, $response)) {
        return $response->withStatus(304);
    }

    $meta = ['curentPage' => $page,
            'prevPage' => (($page - 1) <= 0) ? 1 : ($page - 1),
            'nextPage' => (($page + 1) >= $pageCount) ? $pageCount : ($page + 1),
            'pageSize' => $pageSize,
            'pageCount' => $pageCount,
            'orderBy' => $orderBy . ' ' . $direction ];

    // $this->logger->addInfo("Jumlah record: ". $recordCount);
    
    if ($blnPagination) {
        $point =  $point->offset($offset)->limit($pageSize);
    }

        
    /* Serialize the response data. */
    $fractal = new Manager();
    $fractal->setSerializer(new DataArraySerializer);
    $pointTransformer = new PointTransformer();
    $pointTransformer->setMiddleUrl(getenv("MIDDLE_URL"));
    $resource = new Collection($point, $pointTransformer);
    $data = $fractal->createData($resource)->toArray();
    if ($blnPagination) {
        $data['meta'] = $meta;
    }

    return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
});

$app->post(getenv("MIDDLE_URL") . "/point", function ($request, $response, $arguments) {
    if (false !== $point = $this->spot->mapper("App\Point")->first([
        "user_id" => $this->token->decoded->uid
    ])){
        throw new NotFoundException("Point already created.", 404);
    }
        $point = new Point([
            "user_id" => $this->token->decoded->uid,
            "status" => 1
        ]);
        $this->spot->mapper("App\Point")->save($point); 

    /* Serialize the response data. */
    $fractal = new Manager();
    $fractal->setSerializer(new DataArraySerializer);
    $pointTransformer = new PointTransformer();
    $pointTransformer->setMiddleUrl(getenv("MIDDLE_URL"));
    $resource = new Item($point, $pointTransformer);
    $data = $fractal->createData($resource)->toArray();
    $data["status"] = "ok";
    $data["message"] = "New point created";

    return $response->withStatus(201)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
});

$app->get(getenv("MIDDLE_URL") . "/point/{id}", function ($request, $response, $arguments) {

    

    /* Load existing point using provided id */
    if (false === $point = $this->spot->mapper("App\Point")->first([
        "point_id" => $arguments["id"]
    ])) {
        throw new NotFoundException("Point not found.", 404);
    };

    /* Add Last-Modified and ETag headers to response. */
    $response = $this->cache->withEtag($response, $point->etag());
    $response = $this->cache->withLastModified($response, $point->timestamp());

    /* If-Modified-Since and If-None-Match request header handling. */
    /* Heads up! Apache removes previously set Last-Modified header */
    /* from 304 Not Modified responses. */
    if ($this->cache->isNotModified($request, $response)) {
        return $response->withStatus(304);
    }

    $data["status"] = "ok";    
    /* Serialize the response data. */
    $fractal = new Manager();
    $fractal->setSerializer(new DataArraySerializer);
    $pointTransformer = new PointTransformer();
    $pointTransformer->setMiddleUrl(getenv("MIDDLE_URL"));
    $resource = new Item($point, $pointTransformer);
    $profile = $fractal->createData($resource)->toArray();

    $data = array_merge($data, $profile);
    
    return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
});

$app->delete(getenv("MIDDLE_URL") . "/point/{id}", function ($request, $response, $arguments) {
    
    
    
    /* Load existing point using provided uid */
    if (false === $point = $this->spot->mapper("App\Point")->first([
        "point_id" => $arguments["id"]
        ])) {
            throw new NotFoundException("Point not found.", 404);
        };
        
        $this->spot->mapper("App\Point")->delete($point);
        
        $data["status"] = "ok";
        $data["message"] = "Point deleted";
        
        return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
});
    
