<?php

use App\History;
use App\HistoryTransformer;
use App\Point;
use App\PointTransformer;

use Exception\NotFoundException;
use Exception\ForbiddenException;
use Exception\PreconditionFailedException;
use Exception\PreconditionRequiredException;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\DataArraySerializer;

$app->get(getenv("MIDDLE_URL") . "/history", function ($request, $response, $arguments) {


    // get point account
    $body = $request->getParsedBody();
    $uid = $this->token->decoded->uid;
    $point = false;    
    try {
        $point = $this->spot->mapper("App\Point")->all()->where(["user_id" => $uid])->first(); 
    }
    catch (Exception $e) {
        throw new NotFoundException("Point account is not found", 404);
    }

    if (!$point) {
        throw new NotFoundException("Point account is not found", 404);
    }
        
    
    $fractal = new Manager();
    $fractal->setSerializer(new DataArraySerializer);
    $pointTransformer = new PointTransformer();
    $pointTransformer->setMiddleUrl(getenv("MIDDLE_URL"));
    $res = new Item($point, $pointTransformer);
    $pointData = $fractal->createData($res)->toArray(); 

    $history = $point->history;
    
    $historyTransformer = new HistoryTransformer();
    $historyTransformer->setMiddleUrl(getenv("MIDDLE_URL"));
    $resource = new Collection($history, $historyTransformer);
    $historyData = $fractal->createData($resource)->toArray();
    $data["point"] = $pointData["data"];
    $data["point"]["history"] = $historyData["data"];  
    $data["status"] = "ok";
    $data["message"] = "Successfully get history";

    return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

});