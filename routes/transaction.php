<?php

use App\History;
use App\HistoryTransformer;
use App\Point;
use App\PointTransformer;

use Exception\NotFoundException;
use Exception\ForbiddenException;
use Exception\PreconditionFailedException;
use Exception\PreconditionRequiredException;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\DataArraySerializer;

$app->post(getenv("MIDDLE_URL") . "/deduct", function ($request, $response, $arguments) {

    $body = $request->getParsedBody();
    $user_id = $body["user_id"];
    $point = false;    
    try {
        $point = $this->spot->mapper("App\Point")->all()->where(["user_id" => $user_id])->first(); 
    }
    catch (Exception $e) {
        throw new NotFoundException("point account is not found", 404);
    }

    if (!$point) {
        throw new NotFoundException("point account is not found", 404);
    }
    
    $refNo = $body["trx_id"];
    $refDate = $body["trx_date"] == null ? null : new \Datetime($body["trx_date"]);
    $description = $body["trx_no"];
    $amount = abs($body["amount"]);
    
    // create new history
    $history = new History([
        "point_id" => $point->id,
        "amount" => -$amount, // can be + or -
        "balance" => $point->balance - $amount, // can be + or -
        "type" => 0, // deduct = 0 or topup = 1
        "description" => $description,
        "ref_no" => $refNo, // history guid or bank trasaction no 
        "ref_date" => $refDate, // history guid or bank trasaction no 
        "status" => 1,
    ]);

    $connection = $this->spot->mapper("App\Point")->connection();
    $connection->beginTransaction();    
    try {
        $this->spot->mapper("App\History")->save($history);
    } catch(Exception $e) {
        $connection->rollback();
        $this->logger->addInfo($e->getMessage());
        throw new ForbiddenException($e->getMessage(), 400);
    }

    $point->balance = $history->balance;
    $point->last_deduct = new \DateTime();

    try {
        $this->spot->mapper("App\Point")->save($point);
    } catch (Exception $e) {
        $connection->rollback();
        throw new ForbiddenException($e->getMessage(), 400);
    }

    $connection->commit();

    $fractal = new Manager();
    $fractal->setSerializer(new DataArraySerializer);
    $historyTransformer = new HistoryTransformer();
    $historyTransformer->setMiddleUrl(getenv("MIDDLE_URL"));
    $resource = new Item($history, $historyTransformer);
    $historyData = $fractal->createData($resource)->toArray();
    $data = $historyData; 
    $data["status"] = "ok";
    $data["message"] = "Point deducted successfully";

    return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

});

$app->post(getenv("MIDDLE_URL") . "/topup", function ($request, $response, $arguments) {
    
    // get point  account
    
    $body = $request->getParsedBody();
    $user_id = $body["user_id"];
    $point = false;    
    try {
        $point = $this->spot->mapper("App\Point")->all()->where(["user_id" => $user_id])->first(); 
    }
    catch (Exception $e) {
        throw new NotFoundException("Point account is not found", 404);
    }

    if (!$point) {
        throw new NotFoundException("Point account is not found", 404);
    }
    
    $refNo = $body["trx_id"];
    $refDate = $body["trx_date"] == null ? null : new \Datetime($body["trx_date"]);
    $description = $body["bank_name"].",".$body["bank_account_no"] ;
    $amount = abs($body["amount"]);
    
    // create new history
    $history = new History([
        "point_id" => $point->id,
        "amount" => $amount, // can be + or -
        "balance" => $point->balance + $amount, // can be + or -
        "type" => 1, // deduct = 0 or topup = 1
        "description" => $description,
        "ref_no" => $refNo, // history guid or bank trasaction no 
        "ref_date" => $refDate, // history guid or bank trasaction no 
        "status" => 1,
    ]);

    $connection = $this->spot->mapper("App\Point")->connection();
    $connection->beginTransaction();    
    try {
        $this->spot->mapper("App\History")->save($history);
    } catch(Exception $e) {
        $connection->rollback();
        $this->logger->addInfo($e->getMessage());
        throw new ForbiddenException($e->getMessage(), 400);
    }

    $point->balance = $history->balance;
    $point->last_topup = new \DateTime();

    try {
        $this->spot->mapper("App\Point")->save($point);
    } catch (Exception $e) {
        $connection->rollback();
        throw new ForbiddenException($e->getMessage(), 400);
    }

    $connection->commit();

    $fractal = new Manager();
    $fractal->setSerializer(new DataArraySerializer);
    $historyTransformer = new HistoryTransformer();
    $historyTransformer->setMiddleUrl(getenv("MIDDLE_URL"));
    $resource = new Item($history, $historyTransformer);
    $historyData = $fractal->createData($resource)->toArray();
    $data = $historyData; 
    $data["status"] = "ok";
    $data["message"] = "History top up successfully";

    return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

});

