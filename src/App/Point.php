<?php

namespace App;

use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

use Spot\EventEmitter;
use Tuupola\Base62;
use Psr\Log\LogLevel;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;



class Point extends \Spot\Entity
{
    protected static $table = "point";

    public static function fields()
    {
        return [
            "id" => ["type" => "integer", "unsigned" => true, "primary" => true, "autoincrement" => true],
            "point_id" => ["type" => "guid", "required" => true, "unique" => true],
            "user_id" => ["type" => "guid", "required" => true, "unique" => true],
            "balance" => ["type" => "decimal", "required" => true, "value" => 0],
            "last_topup" => ["type" => "datetime"],
            "last_deduct" => ["type" => "datetime",],
            "status" => ["type" => "smallint", "length" => 6, "required" => true],
            "created_at"   => ["type" => "datetime", "value" => new \DateTime(), "required" => true],
            "updated_at"   => ["type" => "datetime", "value" => new \DateTime(), "required" => true],
        ];
    }

    public static function events(EventEmitter $emitter)
    {
        $emitter->on("beforeInsert", function (Entity $entity, Mapper $mapper) {
            // $entity->uid = (new Base62)->encode(random_bytes(9));
            $uuid = Uuid::uuid4();
            $entity->point_id = $uuid;

        });

        $emitter->on("beforeUpdate", function (Entity $entity, Mapper $mapper) {
            $entity->updated_at = new \DateTime();
        });
    }

    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'history' => $mapper->hasMany($entity, 'App\History', 'point_id'),
        ];
    }

    public function timestamp()
    {
        return $this->updated_at->getTimestamp();
    }

    public function etag()
    {
        return md5($this->filename . $this->timestamp());
    }

    public function clear()
    {
        $this->data([
            "id" => null,
            "point_id" => null,
            "user_id" => null,
            "balance" => null,
            "last_topup" => null,
            "last_deduct" => null,
            "status" => null,
           
        ]);
    }
}
